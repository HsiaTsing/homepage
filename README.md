##Qing Xia
  
---

I'm a Ph.D. candidate of computer science. I'm interested in computer graphics, geometry modelling, shape analysis and computational geometry.
 
**Email**

<neijiangxiaqing@gmail.com>; <xiaqing@buaa.edu.cn>

**Organization**  

[State Key Laboratory of Virtual Reality Technology and System][VRLAB]  
[School of Computer Science and Engineering][SCSE] & [School of Advanced Engineering][SAE]  
[Beihang University][BUAA]

[VRLAB]: http://vrlab.buaa.edu.cn
[SCSE]: http://scse.buaa.edu.cn
[SAE]: http://sae.buaa.edu.cn
[BUAA]: http://www.buaa.edu.cn

**Address**  

XueYuan Road No.37, HaiDian District, Beijing  
100191, China

###Education

---

1. From Sept. 2012 to now, Ph.D. student in School of Computer Science and Engineering at Beihang University.
    * Advisor: Prof. Aimin Hao
2. From Sept. 2012 to now, Ph.D. student in School of Advanced Engineering at Beihang University.
    * An elite program (less than 30 students selected from all Ph.D. students of different majors, only 3 in CS)
3. From Sept. 2008 to Jul. 2012, undergraduate student in School of Computer Science and Engineering at Beihang University.
    * Grade point/average: top 10% (about 190 students)
    * Recmmended for admission to the Ph.D. program without exams
4. From Sept. 2005 to Jun. 2008, senior high school student in No.6 middle school of Neijiang in Sichuan Province.
    * National Matriculation Examination score: 680/750 (top 400 in Sichuan)
5. From Sept. 2002 to Jun. 2005, junior high school student in Dongxing middle school of Neijiang in Sichuan Province.
    * Senior High School Entrance Examination score: 571/625 (2nd in Dongxing district)

###Publications

---

**Conference**

* XXX

**Journal**

* XXX

###Projects

---

1. Screen space SPH fluid rendering 
2. Real-time X-Ray simulator
3. 4D shape sequence completion

###Professional Skills

---

**Progrmming**

* Writing readable code in `C/C++`, `Matlab`, `Python` with daily practical experiences.
* Expert in computer graphics with a deep understanding of graphical rendering pipeline,  implemented many fantastic graphics programs using `OpenGL`.
* Familiar with parallel computing and skillful at using `CUDA` to achieve parallel programs on GPUs.
* Data visualiztion and GUI design.

**Algorithm design, analysis and implementation**

* Experinced with implementing algorithms based on reseach papers.
* Algorithm evaluation and analysis.

**Tools & Environment**

* Editor/IDE: Visual Studio, Eclipse, Qt Creator, gDEBugger, Notepad++, etc.
* Operating systems: Windows, Linux.
* Modelling tools: Blender, 3ds Max, Amira, ZBrush, MeshLab.
* Others: Photoshop, Premiere, Office.